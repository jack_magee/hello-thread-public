#include <iostream> 
#include <thread>


void hello()
{
    std::cout << "Thread Print\n";
}

int main(int argc, char **argv)
{
    std::thread t(hello);
    std::cout << "Main Print\n";
    t.join();
    return 0;
}